<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'proyecto';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre','fecha_inicio', 'fecha_final', 'sector_comercial_id', 'estado_id', 'empleado_id'];

    public function sectorComercial() {
        return $this->belongsTo('App\Models\SectorComercial');
    }
    public function estado() {
        return $this->belongsTo('App\Models\Estado');
    }
    public function empleado() {
        return $this->belongsTo('App\Models\Empleado');
    }
    public function tareas() {
        return $this->hasMany('App\Models\Tarea');
    }
}