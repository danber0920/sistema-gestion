<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Documento extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;                        
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'documento';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre','descripcion', 'tipo', 'ruta','tarea_id'];

    public function tarea() {
        return $this->belongsTo('App\Models\Tarea');
    }
    
    public function versionesDocumento() {
        return $this->hasMany('App\Models\VersionDocumento');
    }
}