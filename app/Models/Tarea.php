<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tarea';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['descripcion', 'tipo', 'duracion_estimada', 'duracion_real', 'fecha_inicio_estimada', 'fecha_inicio_real', 'empleado_id', 'proyecto_id'];

    public function proyecto() {
        return $this->belongsTo('App\Models\Proyecto');
    }
    public function empleado() {
        return $this->belongsTo('App\Models\Empleado');
    }
    public function documentos() {
        return $this->hasMany('App\Models\Documento');
    }
}