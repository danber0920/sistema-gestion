<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'empleado';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['documento','nombre','apellido','direccion','telefono','correo_electronico','fecha_contratacion'];

    public function tareas() {
        return $this->hasMany('App\Models\Tarea');
    }
    public function proyectos() {
        return $this->hasMany('App\Models\Proyecto');
    }
}