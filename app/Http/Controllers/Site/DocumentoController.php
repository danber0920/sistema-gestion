<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tarea;
use App\Models\Documento;
use App\Models\VersionDocumento;
use \Auth;
use \File;

class DocumentoController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($tareaId) {
        $tarea = Tarea::findOrFail($tareaId);
        return view('documento.index', ['tarea' => $tarea]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($tareaId) {
        $tarea = Tarea::findOrFail($tareaId);
        return view('documento.create', [
            'tarea' => $tarea,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($tareaId, Request $request) {
        $tarea = Tarea::findOrFail($tareaId);
        $rules = [
            'nombre' => 'required|string|max:255',
            'descripcion' => 'required|string|max:255',
            'tipo' => 'required|string|max:255',
            'documento' => 'required|file',
        ];
        $this->validate($request, $rules);
        $data = $request->all();
        $data['tarea_id'] = $tareaId;
        $file = $request->file('documento');
        $filename = md5($data["nombre"].  time()).".".$file->getClientOriginalExtension();
        $file->move(public_path( "/storage/".$tarea->proyecto->id."/".$tarea->id), $filename);
        $data["ruta"] = "/storage/".$tarea->proyecto->id."/".$tarea->id."/".$filename;
        $documento = Documento::create($data);
        $data['documento_id'] = $documento->id;
        $version = VersionDocumento::create($data);
        return redirect('/tarea/'.$tareaId.'/documento');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($tareaId, $id) {
        $tarea = Tarea::findOrFail($tareaId);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($tareaId, $id) {
        $tarea = Tarea::findOrFail($tareaId);
        $documento = Documento::findOrFail($id);
        return view('documento.edit', [
            'documento' => $documento,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($tareaId, Request $request, $id) {
        $tarea = Tarea::findOrFail($tareaId);
        $documento = Documento::findOrFail($id);
        $rules = [
            'nombre' => 'required|string|max:255',
            'descripcion' => 'required|string|max:255',
            'tipo' => 'required|string|max:255',
            'documento' => 'required|file',
        ];
        $this->validate($request, $rules);
        $data = $request->all();
        $file = $request->file('documento');
        $filename = md5($data["nombre"].  time()).".".$file->getClientOriginalExtension();
        $file->move(public_path( "/storage/".$tarea->proyecto->id."/".$tarea->id), $filename);
        $data["ruta"] = "/storage/".$tarea->proyecto->id."/".$tarea->id."/".$filename;
        $documento->update($data);
        $data['documento_id'] = $documento->id;
        $version = VersionDocumento::create($data);
        return redirect('/tarea/'.$tareaId.'/documento');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($tareaId, $id) {
        $tarea = Tarea::findOrFail($tareaId);
        $documento = Documento::findOrFail($id);
        foreach($documento->versionesDocumento as $version){
            File::delete(public_path($version->ruta));
        }
        $documento->delete();
        return redirect('/tarea/'.$tareaId.'/documento');
    }
    
    public function version($tareaId, $id) {
        $tarea = Tarea::findOrFail($tareaId);
        $documento = Documento::findOrFail($id);
        return view('documento.version', [
            'documento' => $documento,
        ]);
    }

}
