<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tarea;
use App\Models\Proyecto;
use App\Models\Empleado;
use \Auth;

class TareaController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($proyectoId) {
        $proyecto = Proyecto::findOrFail($proyectoId);
        return view('tarea.index', ['proyecto' => $proyecto]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($proyectoId) {
        $proyecto = Proyecto::findOrFail($proyectoId);
        $empleados = Empleado::all();
        return view('tarea.create', [
            'proyecto' => $proyecto,
            'empleados' => $empleados
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($proyectoId, Request $request) {
        $proyecto = Proyecto::findOrFail($proyectoId);
        $rules = [
            'descripcion' => 'required|string|max:255',
            'tipo' => 'required|string|max:255',
            'duracion_estimada' => 'required|string|max:255',
            'fecha_inicio_estimada' => 'required|date',
        ];
        $this->validate($request, $rules);
        $data = $request->all();
        $data['proyecto_id'] = $proyectoId;
        $tarea = Tarea::create($data);
        return redirect('/proyecto/'.$proyectoId.'/tarea');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($proyectoId, $id) {
        $proyecto = Proyecto::findOrFail($proyectoId);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($proyectoId, $id) {
        $proyecto = Proyecto::findOrFail($proyectoId);
        $tarea = Tarea::findOrFail($id);
        $empleados = Empleado::all();
        return view('tarea.edit', [
            'tarea' => $tarea,
            'empleados' => $empleados,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($proyectoId, Request $request, $id) {
        $proyecto = Proyecto::findOrFail($proyectoId);
        $tarea = Tarea::findOrFail($id);
        $rules = [
            'descripcion' => 'required|string|max:255',
            'tipo' => 'required|string|max:255',
            'duracion_real' => 'required|string|max:255',
            'fecha_inicio_real' => 'required|date',
            'empleado_id' => 'required|exists:empleado,id',
        ];
        $this->validate($request, $rules);
        $data = $request->all();
        $tarea->update($data);
        return redirect('/proyecto/'.$proyectoId.'/tarea');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($proyectoId, $id) {
        $proyecto = Proyecto::findOrFail($proyectoId);
        $tarea = Tarea::findOrFail($id)->delete();
        return redirect('/proyecto/'.$proyectoId.'/tarea');
    }

}
