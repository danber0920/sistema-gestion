<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Empleado;
use \Auth;

class EmpleadoController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $empleados = Empleado::all();
        return view('empleado.index', ['empleados' => $empleados]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('empleado.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $rules = [
            'documento' => 'required|string|max:255|unique:empleado',
            'nombre' => 'required|string|max:255',
            'apellido' => 'required|string|max:255',
            'direccion' => 'required|string|max:255',
            'telefono' => 'required|string|max:255',
            'correo_electronico' => 'required|string|email|max:255|unique:empleado',
            'fecha_contratacion' => 'required|date',
        ];
        $this->validate($request, $rules);
        $data = $request->all();
        $empleado = Empleado::create($data);
        return redirect('/empleado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $empleado = Empleado::findOrFail($id);
        return view('empleado.edit', ['empleado' => $empleado]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $empleado = Empleado::findOrFail($id);
        $rules = [
            'documento' => 'required|string|max:255|unique:empleado,documento,' . $empleado->id,
            'nombre' => 'required|string|max:255',
            'apellido' => 'required|string|max:255',
            'direccion' => 'required|string|max:255',
            'telefono' => 'required|string|max:255',
            'correo_electronico' => 'required|string|email|max:255|unique:empleado,correo_electronico,' . $empleado->id,
            'fecha_contratacion' => 'required|date',
        ];
        $this->validate($request, $rules);
        $data = $request->all();
        $empleado->update($data);
        return redirect('/empleado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $empleado = Empleado::findOrFail($id)->delete();
        return redirect('/empleado');
    }

}
