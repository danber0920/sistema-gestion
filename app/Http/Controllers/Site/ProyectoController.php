<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Proyecto;
use App\Models\SectorComercial;
use App\Models\Estado;
use App\Models\Empleado;
use \Auth;

class ProyectoController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $proyectos = Proyecto::all();
        return view('proyecto.index', ['proyectos' => $proyectos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $sectoresComerciales = SectorComercial::all();
        $empleados = Empleado::all();
        return view('proyecto.create', [
            'sectoresComerciales' => $sectoresComerciales,
            'empleados' => $empleados,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $rules = [
            'nombre' => 'required|string|max:255',
            'fecha_inicio' => 'required|date|string|max:255',
            'fecha_final' => 'required|date|string|max:255',
            'sector_comercial_id' => 'required|string|max:255|exists:sector_comercial,id',
            'empleado_id' => 'required|string|max:255|exists:empleado,id',
        ];
        $this->validate($request, $rules);
        $data = $request->all();
        $data["estado_id"] = 1;
        $proyecto = Proyecto::create($data);
        return redirect('/proyecto');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $proyecto = Proyecto::findOrFail($id);
        $sectoresComerciales = SectorComercial::all();
        $empleados = Empleado::all();
        $estados = Estado::all();
        return view('proyecto.edit', [
            'proyecto' => $proyecto,
            'sectoresComerciales' => $sectoresComerciales,
            'empleados' => $empleados,
            'estados' => $estados,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $proyecto = Proyecto::findOrFail($id);
        $rules = [
            'nombre' => 'required|string|max:255',
            'fecha_inicio' => 'required|date|string|max:255',
            'fecha_final' => 'required|date|string|max:255',
            'sector_comercial_id' => 'required|string|max:255|exists:sector_comercial,id',
            'empleado_id' => 'required|string|max:255|exists:empleado,id',
            'estado_id' => 'required|string|max:255|exists:estado,id',
        ];
        $this->validate($request, $rules);
        $data = $request->all();
        $proyecto->update($data);
        return redirect('/proyecto');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $proyecto = Proyecto::findOrFail($id)->delete();
        return redirect('/proyecto');
    }

}
