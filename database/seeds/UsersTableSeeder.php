<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'name' => "Admin",
            'email' => "admin@sisges.com",
            'password' => \Hash::make('sisges1'),
            'active' => 1,
            'api_token' => str_random(60),
            'created_at' => new Datetime,
            'updated_at' => new Datetime,
        ]);
    }
}
