<?php

use Illuminate\Database\Seeder;

class EstadoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estado')->insert([
            'id' => 1,
            'nombre' => "Sin Iniciar",
        ]);
        DB::table('estado')->insert([
            'id' => 2,
            'nombre' => "En Proceso",
        ]);
        DB::table('estado')->insert([
            'id' => 3,
            'nombre' => "Terminado",
        ]);
    }
}
