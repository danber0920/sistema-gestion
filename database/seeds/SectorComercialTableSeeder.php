<?php

use Illuminate\Database\Seeder;

class SectorComercialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sector_comercial')->insert([
            'id' => 1,
            'nombre' => "Público",
        ]);
        DB::table('sector_comercial')->insert([
            'id' => 2,
            'nombre' => "Privado",
        ]);
        DB::table('sector_comercial')->insert([
            'id' => 3,
            'nombre' => "Comercial",
        ]);
        DB::table('sector_comercial')->insert([
            'id' => 4,
            'nombre' => "Educativo",
        ]);
    }
}
