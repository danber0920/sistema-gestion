<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTareaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tarea', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->string('tipo');
            $table->string('duracion_estimada');
            $table->string('duracion_real')->nullable();
            $table->date('fecha_inicio_estimada');
            $table->date('fecha_inicio_real')->nullable();
            $table->integer('empleado_id')->unsigned()->nullable();
            $table->integer('proyecto_id')->unsigned();

            $table->foreign('empleado_id')->references('id')->on('empleado')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('proyecto_id')->references('id')->on('proyecto')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('response');
    }
}
