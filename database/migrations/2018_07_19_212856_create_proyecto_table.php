<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyectoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyecto', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->date('fecha_inicio');
            $table->date('fecha_final');
            $table->integer('sector_comercial_id')->unsigned();
            $table->integer('estado_id')->unsigned();
            $table->integer('empleado_id')->unsigned();

            $table->foreign('sector_comercial_id')->references('id')->on('sector_comercial')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('estado_id')->references('id')->on('estado')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('empleado_id')->references('id')->on('empleado')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('picking');
    }
}
