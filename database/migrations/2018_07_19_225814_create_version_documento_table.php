<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVersionDocumentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('version_documento', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->string('ruta');
            $table->timestamps();
            $table->integer('documento_id')->unsigned();

            $table->foreign('documento_id')->references('id')->on('documento')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status');
    }
}
