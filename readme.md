#Como Instalar en Xampp

Primero se debe clonar el proyecto en la carpeta htdocs


```sh
    cd C:/xampp/htdocs
    git clone git@bitbucket.org:danber0920/sistema-gestion.git sisges
```

Luego se añade al archivo htdocs con la siguiente configuración
```sh
    <VirtualHost sisges.dev:80>
      DocumentRoot "C:/xampp/htdocs/sisges/public"
      ServerAdmin sisges.dev
      <Directory "C:/xampp/htdocs/sisges">
            Options Indexes FollowSymLinks
            AllowOverride All
            Require all granted
      </Directory>
    </VirtualHost>
```

A continuación en la base de datos creamos el schema sisges

```sh
    CREATE DATABASE sisges;
```

E importamos el archivo C:\xampp\htdocs\sisges\sisges.sql
Despues procedemos a abrir el archivo C:\Windows\System32\drivers\etc\hosts como administrador y agregamos la linea

```sh
    127.0.0.1	sisges.dev
```

Nos aseguramos de tener bien la información de la conexion a la base de datos (DB) en el archivo C:\xampp\htdocs\sisges\.env
Por último corremos los comandos


```sh
    cd C:\xampp\htdocs\sisges
    php artisan key:generate
```

Y con esto podremos acceder a la página sisges.dev, con los accesos

```sh
    Usuario: admin@sisges.com
    Contraseña: sisges1
```