@extends('base.base_layout', [
'header_anonymous'  => 0,
'header_auth'       => 1,
'menu_main'         => 1,
'aside_left'        => 0,
'aside_right'       => 0,
'layout_bottom'     => 0,
'layout_bottom_a'   => 0,
'layout_bottom_b'   => 0,
'layout_bottom_c'   => 0,
'modal_count'       => 0,
])
@section('class-html', '')
@section('title', 'Login')
@section('description-meta', '')
@section('aditional-meta')
<!-- Add meta tags to this page -->
@stop
@section('aditional-css')
<!-- Add link tags to this page -->
@stop
@section('class-body', 'page-contact')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Crear Documento</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <form role="form" action="/tarea/{{ $tarea->id}}/documento" method="POST" enctype="multipart/form-data">
            <div class="col-lg-6">
                <!-- /.panel-heading -->
                <div class="form-group">
                    <label>Nombre</label>
                    <input name="nombre" value="{{ old('nombre') }}" class="form-control" placeholder="Nombre">
                    <p class="help-block text-danger">{{ $errors->first('nombre') }}</p>
                </div>
                <div class="form-group">
                    <label>Tipo</label>
                    <input name="tipo" value="{{ old('tipo') }}" class="form-control" placeholder="Tipo">
                    <p class="help-block text-danger">{{ $errors->first('tipo') }}</p>
                </div>
            </div>
            <div class="col-lg-6">
                <!-- /.panel-heading -->
                <div class="form-group">
                    <label>Descripción</label>
                    <input name="descripcion" value="{{ old('descripcion') }}" class="form-control" placeholder="Descripción">
                    <p class="help-block text-danger">{{ $errors->first('descripcion') }}</p>
                </div>
                <div class="form-group">
                    <label>Documento</label>
                    <input name="documento" type="file" class="form-control" placeholder="Documento">
                    <p class="help-block text-danger">{{ $errors->first('documento') }}</p>
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-default">Crear Documento</button>
                <a href="/tarea/{{ $tarea->id}}/documento" class="btn btn-default">Volver al listado</a>
            </div>
        </form>
    </div>
</div>
@stop
<!-- If this page has the variable modal_count > 0,
then each modal content is identified with a number (#)-->
@section('modal_content_#')
<!-- Content of modal number # in this page -->
@stop