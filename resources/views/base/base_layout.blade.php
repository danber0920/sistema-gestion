<!DOCTYPE html>
<!--if lt IE 7html.no-js.lt-ie9.lt-ie8.lt-ie7(lang='es')  
-->
<!--if IE 7html.no-js.lt-ie9.lt-ie8(lang='es')  
-->
<!--if IE 8html.no-js.lt-ie9(lang='es')  
-->
<!-- [if gt IE 8] <!-->
<html lang="es" class="no-js">
    <!-- <![endif]-->
    <!-- Insert head html-->
    <head>
        <meta charset="utf-8">
        <!-- SEO-->
        <meta name="description" content="@yield('description-meta')"/>
        <meta name="author" content="International">
        <!-- style app -->
        <meta name="msapplication-TileColor" content="#f77800">
        <meta name="theme-color" content="#f77800">
        <meta name="apple-mobile-web-app-status-bar-style" content="#f77800">
        <title>SisGes - @yield('title')</title>
        <!--<title>Plataforma de promociones International - @yield('title')</title>-->
        <!-- Add meta tags to all project -->
        @yield('aditional-meta')
        <link rel="manifest" href="/manifest.json">
        <!-- Css apply to all project (plugins) -->
        @yield('aditional-css')
        <link rel="stylesheet" href="{{ URL::asset('/vendor/bootstrap/css/bootstrap.min.css') }}"/>
        <link rel="stylesheet" href="{{ URL::asset('/vendor/metisMenu/metisMenu.min.css') }}"/>
        <link rel="stylesheet" href="{{ URL::asset('/dist/css/sb-admin-2.css') }}"/>
        <link rel="stylesheet" href="{{ URL::asset('/vendor/font-awesome/css/font-awesome.min.css') }}"/>
        <link rel="stylesheet" href="{{ URL::asset('/css/app.css') }}"/>
        <link rel="stylesheet" href="{{ URL::asset('/vendor/modernizr/modernizr-2.8.3.min.js') }}"/>
    </head>

    <body class="@yield('class-body')">
        <div id="wrapper">
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                @include('base.header')
                @include('base.menu_main')
            </nav>
            @include('base.main_content')
            <footer class="site-footer">
                @include('base.footer')
            </footer>
            @include('base.modal')
            <script src="{{ URL::asset('/vendor/jquery/jquery.min.js') }}"></script>
            <script src="{{ URL::asset('/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
            <script src="{{ URL::asset('/vendor/metisMenu/metisMenu.min.js') }}"></script>
            <script src="{{ URL::asset('/dist/js/sb-admin-2.js') }}"></script>
            <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\\/script>')</script>
            <!-- Js apply to all project (plugins) -->
            @yield('aditional-js')
        </div>
    </body>
</html>