@extends('base.base_layout', [
'header_anonymous'  => 0,
'header_auth'       => 1,
'menu_main'         => 1,
'aside_left'        => 0,
'aside_right'       => 0,
'layout_bottom'     => 0,
'layout_bottom_a'   => 0,
'layout_bottom_b'   => 0,
'layout_bottom_c'   => 0,
'modal_count'       => 0,
])
@section('class-html', '')
@section('title', 'Login')
@section('description-meta', '')
@section('aditional-meta')
<!-- Add meta tags to this page -->
@stop
@section('aditional-css')
<!-- Add link tags to this page -->
@stop
@section('class-body', 'page-contact')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Editar Proyecto</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <form role="form" action="/proyecto/{{ $proyecto->id }}" method="POST">
            <div class="col-lg-6">
                <!-- /.panel-heading -->
                <div class="form-group">
                    <label>Nombre</label>
                    <input name="nombre" value="{{ old('nombre')?old('nombre'):$proyecto->nombre }}" class="form-control" placeholder="Nombre">
                    <p class="help-block text-danger">{{ $errors->first('nombre') }}</p>
                </div>
                <div class="form-group">
                    <label>Fecha Inicio (AAAA-MM-DD)</label>
                    <input name="fecha_inicio" value="{{ old('fecha_inicio')?old('fecha_inicio'):$proyecto->fecha_inicio }}" class="form-control" placeholder="Fecha Inicio">
                    <p class="help-block text-danger">{{ $errors->first('fecha_inicio') }}</p>
                </div>
                <div class="form-group">
                    <label>Lider de Proyecto</label>
                    <select name="empleado_id"  class="form-control">
                        <option value="">Lider de Proyecto...</option>
                        @foreach ($empleados as $empleado)
                            <option value="{{ $empleado->id }}" {{ old('empleado_id')?(old('empleado_id')==$empleado->id?'selected':''):($proyecto->empleado_id==$empleado->id?'selected':'') }}>{{ $empleado->nombre }} {{ $empleado->apellido }}</option>
                        @endforeach
                    </select>
                    <p class="help-block text-danger">{{ $errors->first('empleado_id') }}</p>
                </div>
            </div>
            <div class="col-lg-6">
                <!-- /.panel-heading -->
                <div class="form-group">
                    <label>Sector Comercial</label>
                    <select name="sector_comercial_id"  class="form-control">
                        <option value="">Sector Comercial...</option>
                        @foreach ($sectoresComerciales as $sectorComercial)
                            <option value="{{ $sectorComercial->id }}" {{ old('sector_comercial_id')?(old('sector_comercial_id')==$sectorComercial->id?'selected':''):($proyecto->sector_comercial_id==$sectorComercial->id?'selected':'') }}>{{ $sectorComercial->nombre }}</option>
                        @endforeach
                    </select>
                    <p class="help-block text-danger">{{ $errors->first('sector_comercial_id') }}</p>
                </div>
                <div class="form-group">
                    <label>Fecha Fin (AAAA-MM-DD)</label>
                    <input name="fecha_final" value="{{ old('fecha_final')?old('fecha_final'):$proyecto->fecha_final }}" class="form-control" placeholder="Fecha Fin">
                    <p class="help-block text-danger">{{ $errors->first('fecha_final') }}</p>
                </div>
                <div class="form-group">
                    <label>Estado</label>
                    <select name="estado_id"  class="form-control">
                        <option value="">Estado...</option>
                        @foreach ($estados as $estado)
                            <option value="{{ $estado->id }}" {{ old('estado_id')?(old('estado_id')==$estado->id?'selected':''):($proyecto->estado_id==$estado->id?'selected':'') }}>{{ $estado->nombre }}</option>
                        @endforeach
                    </select>
                    <p class="help-block text-danger">{{ $errors->first('estado_id') }}</p>
                </div>
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <button type="submit" class="btn btn-default">Editar Proyecto</button>
                <a href="/proyecto" class="btn btn-default">Volver al listado</a>
            </div>
        </form>
    </div>
</div>
@stop
<!-- If this page has the variable modal_count > 0,
then each modal content is identified with a number (#)-->
@section('modal_content_#')
<!-- Content of modal number # in this page -->
@stop