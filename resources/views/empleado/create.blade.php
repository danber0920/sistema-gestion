@extends('base.base_layout', [
'header_anonymous'  => 0,
'header_auth'       => 1,
'menu_main'         => 1,
'aside_left'        => 0,
'aside_right'       => 0,
'layout_bottom'     => 0,
'layout_bottom_a'   => 0,
'layout_bottom_b'   => 0,
'layout_bottom_c'   => 0,
'modal_count'       => 0,
])
@section('class-html', '')
@section('title', 'Login')
@section('description-meta', '')
@section('aditional-meta')
<!-- Add meta tags to this page -->
@stop
@section('aditional-css')
<!-- Add link tags to this page -->
@stop
@section('class-body', 'page-contact')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Crear Empleado</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <form role="form" action="/empleado" method="POST">
            <div class="col-lg-6">
                <!-- /.panel-heading -->
                <div class="form-group">
                    <label>Documento</label>
                    <input name="documento" value="{{ old('documento') }}" class="form-control" placeholder="Documento">
                    <p class="help-block text-danger">{{ $errors->first('documento') }}</p>
                </div>
                <div class="form-group">
                    <label>Nombre</label>
                    <input name="nombre" value="{{ old('nombre') }}" class="form-control" placeholder="Nombre">
                    <p class="help-block text-danger">{{ $errors->first('nombre') }}</p>
                </div>
                <div class="form-group">
                    <label>Dirección</label>
                    <input name="direccion" value="{{ old('direccion') }}" class="form-control" class="form-control" placeholder="Dirección">
                    <p class="help-block text-danger">{{ $errors->first('direccion') }}</p>
                </div>
                <div class="form-group">
                    <label>Correo Electrónico</label>
                    <input name="correo_electronico" value="{{ old('correo_electronico') }}" class="form-control" class="form-control" placeholder="Correo Electrónico">
                    <p class="help-block text-danger">{{ $errors->first('correo_electronico') }}</p>
                </div>
            </div>
            <div class="col-lg-6">
                <!-- /.panel-heading -->
                <div class="form-group" style="height: 59px">
                </div>
                <div class="form-group">
                    <label>Apellido</label>
                    <input name="apellido" value="{{ old('apellido') }}" class="form-control" class="form-control" placeholder="Apellido">
                    <p class="help-block text-danger">{{ $errors->first('apellido') }}</p>
                </div>
                <div class="form-group">
                    <label>Teléfono</label>
                    <input name="telefono" value="{{ old('telefono') }}" class="form-control" class="form-control" placeholder="Teléfono">
                    <p class="help-block text-danger">{{ $errors->first('telefono') }}</p>
                </div>
                <div class="form-group">
                    <label>Fecha Contratación (AAAA-MM-DD)</label>
                    <input name="fecha_contratacion" value="{{ old('fecha_contratacion') }}" class="form-control" class="form-control" placeholder="Fecha Contratación">
                    <p class="help-block text-danger">{{ $errors->first('fecha_contratacion') }}</p>
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-default">Crear Empleado</button>
                <a href="/empleado" class="btn btn-default">Volver al listado</a>
            </div>
        </form>
    </div>
</div>
@stop
<!-- If this page has the variable modal_count > 0,
then each modal content is identified with a number (#)-->
@section('modal_content_#')
<!-- Content of modal number # in this page -->
@stop