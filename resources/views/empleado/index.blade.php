@extends('base.base_layout', [
'header_anonymous'  => 0,
'header_auth'       => 1,
'menu_main'         => 1,
'aside_left'        => 0,
'aside_right'       => 0,
'layout_bottom'     => 0,
'layout_bottom_a'   => 0,
'layout_bottom_b'   => 0,
'layout_bottom_c'   => 0,
'modal_count'       => 0,
])
@section('class-html', '')
@section('title', 'Login')
@section('description-meta', '')
@section('aditional-meta')
<!-- Add meta tags to this page -->
@stop
@section('aditional-css')
<!-- Add link tags to this page -->
@stop
@section('class-body', 'page-contact')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Empleados</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <!-- /.panel-heading -->
            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr>
                        <th>Documento</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Dirección</th>
                        <th>Telefono</th>
                        <th>Correo Electrónico</th>
                        <th>Fecha Contratación</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($empleados as $empleado)
                    <tr class="gradeX">
                        <td>{{ $empleado->documento }}</td>
                        <td>{{ $empleado->nombre }}</td>
                        <td>{{ $empleado->apellido }}</td>
                        <td>{{ $empleado->direccion }}</td>
                        <td>{{ $empleado->telefono }}</td>
                        <td>{{ $empleado->correo_electronico }}</td>
                        <td>{{ $empleado->fecha_contratacion }}</td>
                        <td>
                            <form action="/empleado/{{ $empleado->id }}" method="POST"> 
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <a href="/empleado/{{ $empleado->id }}/edit" class="btn btn-xs btn-default"><i class="fa fa-edit fa-fw"></i></a>
                                <button class="btn btn-xs btn-default"><i class="fa fa-trash fa-fw"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <a href="/empleado/create" class="btn btn-default"><i class="fa fa-plus fa-fw"></i> Nuevo Empleado</a>
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
@stop
<!-- If this page has the variable modal_count > 0,
then each modal content is identified with a number (#)-->
@section('modal_content_#')
<!-- Content of modal number # in this page -->
@stop