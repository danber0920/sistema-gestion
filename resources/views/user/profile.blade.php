@extends('base.base_layout', [
'header_anonymous'  => 0,
'header_auth'       => 1,
'menu_main'         => 1,
'aside_left'        => 0,
'aside_right'       => 0,
'layout_bottom'     => 0,
'layout_bottom_a'   => 0,
'layout_bottom_b'   => 0,
'layout_bottom_c'   => 0,
'modal_count'       => 0,
])
@section('class-html', '')
@section('title', 'Login')
@section('description-meta', '')
@section('aditional-meta')
<!-- Add meta tags to this page -->
@stop
@section('aditional-css')
<!-- Add link tags to this page -->
@stop
@section('class-body', 'page-contact')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Mi Perfil</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <form role="form" action="/user/profile" method="POST">
            <div class="col-lg-6">
                <!-- /.panel-heading -->
                <div class="form-group">
                    <label>Nombre</label>
                    <input name="name" value="{{ old('name')?old('name'):$user->name }}" class="form-control" placeholder="Nombre">
                    <p class="help-block text-danger">{{ $errors->first('name') }}</p>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input name="email" value="{{ old('email')?old('email'):$user->email }}" class="form-control" placeholder="Email">
                    <p class="help-block text-danger">{{ $errors->first('email') }}</p>
                </div>
            </div>
            <div class="col-lg-6">
                <!-- /.panel-heading -->
                <div class="form-group">
                    <label>Contraseña</label>
                    <input name="password" type="password" class="form-control" placeholder="Contraseña" value="">
                    <p class="help-block text-danger">{{ $errors->first('password') }}</p>
                </div>
                <div class="form-group">
                    <label>Repetir Contraseña</label>
                    <input name="password_confirmation" type="password" class="form-control" placeholder="Repetir Contraseña">
                </div>
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <button type="submit" class="btn btn-default">Actualizar</button>
                <!--<a href="/user" class="btn btn-default">Volver al listado</a>-->
            </div>
        </form>
    </div>
</div>
@stop
<!-- If this page has the variable modal_count > 0,
then each modal content is identified with a number (#)-->
@section('modal_content_#')
<!-- Content of modal number # in this page -->
@stop