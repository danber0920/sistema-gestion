@extends('base.base_layout', [
'header_anonymous'  => 0,
'header_auth'       => 1,
'menu_main'         => 1,
'aside_left'        => 0,
'aside_right'       => 0,
'layout_bottom'     => 0,
'layout_bottom_a'   => 0,
'layout_bottom_b'   => 0,
'layout_bottom_c'   => 0,
'modal_count'       => 0,
])
@section('class-html', '')
@section('title', 'Login')
@section('description-meta', '')
@section('aditional-meta')
<!-- Add meta tags to this page -->
@stop
@section('aditional-css')
<!-- Add link tags to this page -->
@stop
@section('class-body', 'page-contact')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Tareas - {{ $proyecto->nombre }}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <!-- /.panel-heading -->
            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Descripción</th>
                        <th>Tipo</th>
                        <th>Duración Estimada</th>
                        <th>Duración Real</th>
                        <th>Fecha Inicio Estimada</th>
                        <th>Fecha Inicio Real</th>
                        <th>Empleado</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($proyecto->tareas as $tarea)
                    <tr class="gradeX">
                        <td>{{ $tarea->id }}</td>
                        <td>{{ $tarea->descripcion }}</td>
                        <td>{{ $tarea->tipo }}</td>
                        <td>{{ $tarea->duracion_estimada }}</td>
                        <td>{{ $tarea->duracion_real }}</td>
                        <td>{{ $tarea->fecha_inicio_estimada }}</td>
                        <td>{{ $tarea->fecha_inicio_real }}</td>
                        <td>{{ $tarea->empleado?$tarea->empleado->nombre:'' }} {{ $tarea->empleado?$tarea->empleado->apellido:'' }}</td>
                        <td>
                            <form action="/proyecto/{{ $proyecto->id }}/tarea/{{ $tarea->id}}" method="POST"> 
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <a href="/proyecto/{{ $proyecto->id }}/tarea/{{ $tarea->id}}/edit" class="btn btn-xs btn-default"><i class="fa fa-edit fa-fw"></i></a>
                                <a href="/tarea/{{ $tarea->id }}/documento" class="btn btn-xs btn-default" alt="Ver tareas"><i class="fa fa-file-text fa-fw"></i></a>
                                <button class="btn btn-xs btn-default"><i class="fa fa-trash fa-fw"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <a href="/proyecto/{{ $proyecto->id }}/tarea/create" class="btn btn-default"><i class="fa fa-plus fa-fw"></i> Nueva Tarea</a>
            <a href="/proyecto" class="btn btn-default">Volver a Proyectos</a>
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
@stop
<!-- If this page has the variable modal_count > 0,
then each modal content is identified with a number (#)-->
@section('modal_content_#')
<!-- Content of modal number # in this page -->
@stop