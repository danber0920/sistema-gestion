@extends('base.base_layout', [
'header_anonymous'  => 0,
'header_auth'       => 1,
'menu_main'         => 1,
'aside_left'        => 0,
'aside_right'       => 0,
'layout_bottom'     => 0,
'layout_bottom_a'   => 0,
'layout_bottom_b'   => 0,
'layout_bottom_c'   => 0,
'modal_count'       => 0,
])
@section('class-html', '')
@section('title', 'Login')
@section('description-meta', '')
@section('aditional-meta')
<!-- Add meta tags to this page -->
@stop
@section('aditional-css')
<!-- Add link tags to this page -->
@stop
@section('class-body', 'page-contact')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Editar Tarea</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <form role="form" action="/proyecto/{{ $tarea->proyecto->id}}/tarea/{{ $tarea->id }}" method="POST">
            <div class="col-lg-6">
                <!-- /.panel-heading -->
                <div class="form-group">
                    <label>Descripción</label>
                    <input name="descripcion" value="{{ old('descripcion')?old('descripcion'):$tarea->descripcion }}" class="form-control" placeholder="Descripción">
                    <p class="help-block text-danger">{{ $errors->first('descripcion') }}</p>
                </div>
                <div class="form-group">
                    <label>Duración Real</label>
                    <input name="duracion_real" value="{{ old('duracion_real')?old('duracion_real'):$tarea->duracion_real }}" class="form-control" placeholder="Duración Real">
                    <p class="help-block text-danger">{{ $errors->first('duracion_real') }}</p>
                </div>
                <div class="form-group">
                    <label>Empleado</label>
                    <select name="empleado_id"  class="form-control">
                        <option value="">Empleado...</option>
                        @foreach ($empleados as $empleado)
                            <option value="{{ $empleado->id }}" {{ old('empleado_id')?(old('empleado_id')==$empleado->id?'selected':''):($tarea->empleado_id==$empleado->id?'selected':'') }}>{{ $empleado->nombre }} {{ $empleado->apellido }}</option>
                        @endforeach
                    </select>
                    <p class="help-block text-danger">{{ $errors->first('empleado_id') }}</p>
                </div>
            </div>
            <div class="col-lg-6">
                <!-- /.panel-heading -->
                <div class="form-group">
                    <label>Tipo</label>
                    <input name="tipo" value="{{ old('tipo')?old('tipo'):$tarea->tipo }}" class="form-control" placeholder="Tipo">
                    <p class="help-block text-danger">{{ $errors->first('tipo') }}</p>
                </div>
                <div class="form-group">
                    <label>Fecha Inicio Real (AAAA-MM-DD)</label>
                    <input name="fecha_inicio_real" value="{{ old('fecha_inicio_real')?old('fecha_inicio_real'):$tarea->fecha_inicio_real }}" class="form-control" placeholder="Fecha Inicio Real">
                    <p class="help-block text-danger">{{ $errors->first('fecha_inicio_real') }}</p>
                </div>
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <button type="submit" class="btn btn-default">Editar Tarea</button>
                <a href="/proyecto/{{ $tarea->proyecto->id}}/tarea" class="btn btn-default">Volver al listado</a>
            </div>
        </form>
    </div>
</div>
@stop
<!-- If this page has the variable modal_count > 0,
then each modal content is identified with a number (#)-->
@section('modal_content_#')
<!-- Content of modal number # in this page -->
@stop